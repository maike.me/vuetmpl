import axios from 'axios'
import store from '@/store'

export const HTTP = axios.create({
  baseURL: `http://localhost:1234/`,
  headers: {
    authorization: `${store.getters.username}:${store.getters.token}`
  }
})
