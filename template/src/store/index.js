import Vue from 'vue'
import Vuex from 'vuex'
import VueCookie from 'vue-cookie'

Vue.use(Vuex)
Vue.use(VueCookie)

const state = {
  token: Vue.cookie.get('token'),
  username: Vue.cookie.get('username'),
  theme: Vue.cookie.get('theme')
}

const mutations = {
  setToken (state, token) {
    state.token = token
    Vue.cookie.set('token', token, { expires: '1Y' })
  },
  setUsername (state, username) {
    state.username = username
    Vue.cookie.set('username', username, { expires: '1Y' })
  },
  setTheme (state, theme) {
    state.theme = theme
    Vue.cookie.set('theme', theme, { expires: '1Y' })
  },
  removeToken (state) {
    state.token = null
    Vue.cookie.delete('token')
  },
  removeUsername (state) {
    state.username = null
    Vue.cookie.delete('username')
  }
}

const actions = {
}

const getters = {
  token (state) {
    return state.token
  },
  username: state => state.username,
  theme (state) {
    return state.theme
  }
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
