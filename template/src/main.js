// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import store from '@/store'
import progressbar from '@/components/progressbar.vue'
import vmodal from 'vue-js-modal'
import Snotify from 'vue-snotify'
import 'vue-snotify/styles/material.css'

Vue.config.productionTip = false

Vue.use(vmodal)
Vue.use(Snotify)

const bar = Vue.prototype.$bar = new Vue(progressbar).$mount()
document.body.appendChild(bar.$el)

router.beforeEach((to, from, next) => {
  bar.start()
  next()
})

router.afterEach(transition => {
  bar.finish()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
