import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/views/index'
import About from '@/views/about'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: __dirname,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes: [
    {
      path: '/',
      name: 'views-index',
      component: Index
    },
    {
      path: '/about',
      name: 'views-about',
      component: About
    }
  ]
})
